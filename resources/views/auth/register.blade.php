@extends('layouts.app')

@section('content')
<style>
body {
    background-color: #f0cf5b;
}

html, body {
z-index: -5;
} 

::-webkit-scrollbar {
    width: 8px;
}

::-webkit-scrollbar-thumb {
    background-color: grey;
    border-radius: 50px;
} 
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<div class="container reg_container">
    <div id="reg_content">
        <div class="row">
            <div class="col-lg"></div>
            <div class="col-lg-5">
                <div class="card reg_card">
                    <div class="d-flex justify-content-between">
                        <!-- Logo -->
                        <img src="{{asset('storage/images/logo.png')}}" width="25px" class="reg_logo">

                        <!-- Language -->
                        <div class="language-switch">
                            <button onclick="LangSwitch()" class="lang_button">
                                <img id="langFlag" for="hungarian" src="{{asset('storage/images/hungary.png')}}" width="25px"
                                    alt="Language">
                            </button>
                        </div>
                    </div>

                    <!-- Cím -->
                    <div class="mt-4 pb-3 text-center">
                        <h1 id="titleJS">Registration</h1>
                    </div>


                    <!--{{-- FORM --}}-->

                    <form action="#" method="POST" id="registrationForm">

                        <label class="form-label h5 reg_label" id="per_label">Personal information</label>
                        <hr>
                        <!-- name -->
                        <div class="mb-3">
                            <label for="sex" class="form-label" id="fullname">Full name:</label>
                            <div class="d-flex">
                                <div class="form-group reg-form-group me-4">
                                    <input minlength="4" type="text" class="form-control btn btn-block btn-lg login_input reg_input"
                                        id="firstnameJS" name="forename" placeholder="Firstname" required>
                                </div>
                                <div class="form-group reg-form-group">
                                    <input minlength="4" type="text" class="form-control btn btn-block btn-lg login_input reg_input"
                                    id="lastnameJS" name="surname" placeholder="Lastname">
                                </div>
                            </div>
                        </div>

                        <!-- nem -->
                        <div class="mb-4 mb-3" class="reg_sex">
                            <label for="sex" class="form-label" id="gender">Gender</label>
                            <div class="">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="sex" id="sex" value="1">
                                    <label class="form-check-label" id="man">Man</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="sex" id="sex" value="2">
                                    <label class="form-check-label" id="woman">Woman</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="sex" id="sex" value="3">
                                    <label class="form-check-label" id="other">Other</label>
                                </div>
                            </div>
                        </div>

                        <!-- birthdate -->
                        <div class="mb-3">
                            <label for="Birthdate" class="form-label" id="birthdate">Date of birth:</label>
                            <input type="date" class="form-control btn btn-block btn-lg login_input reg_input" id="Birthdate"
                                name="birthdate">
                        </div>

                        <!-- birthplace -->
                        <div class="mb-3">
                            <label for="BirthplaceCountry" class="form-label" id="countryBirthplace">Place of birth:</label>
                            <select id="BirthplaceCountry" class="form-control btn btn-block btn-lg login_input reg_input"
                                name="Birthplacecountry">
                                <option value="#" disabled selected id="BirthCountry">Please choose</option>
                            </select>
                        </div>

                        <!--Nationality-->
                        <div class="mb-3">
                            <label for="Nationality" class="form-label" id="nationality">Nationality:</label>
                            <select id="Nationality" class="form-control btn btn-block btn-lg login_input reg_input"
                                name="nationality">
                                <option value="#" disabled selected id="NationalityJS">Please choose</option>
                            </select>
                        </div>

                        <!--Native Language-->
                        <div class="mb-3">
                            <label for="NativeLanguage" class="form-label" id="nativeLanguage">Native Language:</label>
                            <select id="NativeLanguage" class="form-control btn btn-block btn-lg login_input reg_input"
                                name="NativeLanguage">
                                <option value="#" disabled selected id="ChooseNativeLanguage">Please choose</option>
                            </select>
                        </div>

                        
                        
                        <!--TAJ number-->
                        <div class="mb-3">
                            <label for="TAJNumber" class="form-label" id="tajNumber">TAJ number:</label>
                            <input type="number" class="form-control btn btn-block btn-lg login_input reg_input" id="TAJNumber"
                            name="TAJNumber">
                        </div>

                        <!-- Education ID -->
                        <div class="mb-3">
                            <label for="EducationId" class="form-label" id="educationId">Education ID:</label>
                            <input type="text" class="form-control btn btn-block btn-lg login_input reg_input"
                            name="EducationId" placeholder="01234567890">
                        </div>

                        <!-- Mother's details -->
                        <div class="d-flex">
                            <div class="mb-3 me-3">
                                <label for="MotherFirstname" class="form-label" id="motherFirstname">Mother's firstname:</label>
                                <input minlength="4" type="text" class="form-control btn btn-block btn-lg login_input reg_input"
                                    id="MotherFirstname" name="MotherFirstname">
                            </div>
                            <div class="mb-3">
                                <label for="MotherLastname" class="form-label" id="motherLastname">Mother's lastname</label>
                                <input minlength="4" type="text" class="form-control btn btn-block btn-lg login_input reg_input"
                                    id="MotherLastname" name="MotherLastname">
                            </div>
                        </div>

                        <!-- phone -->
                        <div class="d-flex">
                            <div class="mb-3 me-4">
                                <label class="form-label" id="phone">Phone:</label>
                                <div class="input-group login_input reg_input">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="prep">+36</span>
                                    </div>
                                    <input name="phone" type="text" placeholder="30 456 7895" data-mask="(99) 999-9999"
                                        class="form-control">
                                </div>
                            </div>

                            <!-- email -->
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label" id="email">Email address:</label>
                                <input type="email" class="form-control btn btn-block btn-lg login_input reg_input"
                                    id="exampleInputEmail1" placeholder="example@gmail.com" aria-describedby="emailHelp"
                                    name="email" required>
                            </div>
                        </div>

                        <!--address -->
                        <label  id="address" class=" form-label h5 reg_label mt-4">Address:</label>
                        <hr>
                        <div class="mb-3">
                            <label for="Country" class="form-label" id="countryAddress">Country:</label>
                            <select id="Country" class="form-control btn btn-block btn-lg login_input reg_input" name="country">
                                <option value="#" disabled selected id="addressCountry">Please choose</option>
                            </select>
                        </div>

                        <!--Postal Code -->
                        <div class="d-flex">
                            <div class="mb-3 me-2" style="width: 30%">
                                <label for="postal" class="form-label" id="postalCodeAddress">Postal Code:</label>
                                <input type="number" class="form-control btn btn-block btn-lg login_input reg_input" id="postal"
                                    name="postal">
                            </div>

                            <!--Street -->
                            <div class="mb-3 me-2">
                                <label for="Street" class="form-label" id="streetAddress">Street:</label>
                                <input type="text" class="form-control btn btn-block btn-lg login_input reg_input" id="Street"
                                    name="street">
                            </div>

                            <!--House number -->
                            <div class="mb-3" style="width: 25%">
                                <label for="HouseNnumber" class="form-label" id="houseNumberAddress">House number:</label>
                                <input type="number" class="form-control btn btn-block btn-lg login_input reg_input" id="HouseNnumber"
                                    name="HouseNnumber">
                            </div>
                        </div>
                        <!-- Study language -->
                        <div class="mb-3">
                            <label for="StudyLanguage" class="form-label" id="studyLanguage">Study language:</label>
                            <select id="StudyLanguage" class="form-control btn btn-block btn-lg login_input reg_input"
                                name="StudyLanguage">
                                <option value="#" disabled selected id="ChooseStudyLanguage">Please choose</option>
                            </select>
                        </div>

                        <!--Type of certificate -->
                        <div class="mb-3">
                            <label for="TypeofCertificate" class="form-label" id="typeOfCertificate">Type of
                                certificate:</label>
                            <select id="TypeofCertificate" class="form-control btn btn-block btn-lg login_input reg_input"
                                name="TypeofCertificate">
                                <option value="#" disabled selected id="typeOfCertificateJS">Please choose</option>
                            </select>
                        </div>

                        <!--Certificate number-->
                        <div class="mb-3">
                            <label for="CertificateNumber" class="form-label" id="certificateNumber">Certificate
                                number:</label>
                            <input type="number" class="form-control btn btn-block btn-lg login_input reg_input"
                                id="CertificateNumber" name="CertificateNumber">
                        </div>

                        <!--Tax number-->
                        <div class="mb-3">
                            <label for="TaxIdNumber" class="form-label" id="taxIdNumber">Tax ID number:</label>
                            <input type="number" class="form-control btn btn-block btn-lg login_input reg_input" id="TaxIdNumber"
                                name="TaxIdNumber">
                        </div>

                        <!--Bank Name-->
                        <div class="mb-3">
                            <label for="AccountHoldingBank" class="form-label" id="accountHoldingBank">Account-holding bank:</label>
                            <input type="text" class="form-control btn btn-block btn-lg login_input reg_input" id="AccountHoldingBank"
                                name="AccountHoldingBank">
                        </div>

                        <!--Bank account number-->
                        <div class="mb-3">
                            <label for="BankAccountNumber" class="form-label" id="bankAccountNumber">Bank account
                                number:</label>
                            <input type="number" class="form-control btn btn-block btn-lg login_input reg_input"
                                id="BankAccountNumber" name="BankAccountNumber">
                        </div>

                        <!--Bank account holder name-->
                        <div class="mb-3">
                            <label for="BankAccountHolderName" class="form-label" id="bankAccountHolderName">Bank account
                                owner name:</label>
                            <input type="text" class="form-control btn btn-block btn-lg login_input reg_input"
                                id="BankAccountHolderName" name="BankAccountHolderName">
                        </div>
                        <hr>
                        <!-- username -->
                        <div class="mb-3">
                            <label for="Username" class="form-label" id="username">Username:</label>
                            <input minlength="4" type="text" class="form-control btn btn-block btn-lg login_input reg_input"
                                id="usernameJS" name="username" placeholder="Alma1234">
                        </div>

                        <!-- password -->
                        <div class="passwordParent mb-3" style="position: relative">
                            <label for="exampleInputPassword1" class="form-label" id="password">Password:</label>
                            <input type="password" class="form-control btn btn-block btn-lg login_input reg_input"
                                id="exampleInputPassword1" name="password" minlength="8"
                                data-parsley-pattern="^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$">
                            <span class="reg_eye">
                                <i class="fa-solid fa-eye passwordIcon"
                                    onclick="passwordVisibility('exampleInputPassword1', this)"></i>
                            </span>
                        </div>
                        <div class="passwordParent mb-3" style="position: relative">
                            <label for="exampleInputPassword1" class="form-label" id="passwordconfirm">Password
                                confirm:</label>
                            <input type="password" class="form-control btn btn-block btn-lg login_input reg_input"
                                id="exampleInputPassword2" name="passwordConfirm"
                                data-parsley-equalto="exampleInputPassword1">
                                <span class="reg_eye">
                                    <i class="fa-solid fa-eye passwordIcon" onclick="passwordVisibility('exampleInputPassword2', this)"></i>
                                </span>
                        </div>

                        <div class="text-center mb-3">
                            <button type="submit" class="btn btn-block btn-lg submit_button mb-3" name="submit"
                                id="regsitration">Registration</button>
                            <p id="signInJS">
                                Already have an account?  <a href="{{route('login')}}" class="reg_link">Sign In</a>
                            </p>
                        </div>

                    </form>
                </div>
            </div>
            {{-- <div class="col-lg-5">
                <img src="..\assets\images\AI generált képek\24b6d10a-2158-40c4-a289-f0aca08095b3.jpg" alt="" class="register_image">
            </div> --}}
            <div class="col-lg"></div>
        </div>
    </div>
</div>

<script>
    let isShown = false;
    function passwordVisibility(inputId, icon) {

        let icon1 = icon;
        let passwordInput = document.getElementById(inputId);
        if (!isShown) {
            icon1.setAttribute("class", "svg-inline--fa fa-eye-slash passwordIcon");
            passwordInput.setAttribute("type", "text");
            isShown = true;
        }
        else {
            icon1.setAttribute("class", "svg-inline--fa fa-eye passwordIcon");
            passwordInput.setAttribute("type", "password");
            isShown = false;
        }
    }
    /* Parsley magyar*/
let langflag = document.getElementById("langFlag");
if(langflag.getAttribute('for' == "hungarian")){
    /*Parsley.addMessages('en', {
        defaultMessage: "This value seems to be invalid.",
        type: {
            email: "This value should be a valid email.",
            url: "This value should be a valid url.",
            number: "This value should be a valid number.",
            integer: "This value should be a valid integer.",
            digits: "This value should be digits.",
            alphanum: "This value should be alphanumeric."
        },
        notblank: "This value should not be blank.",
        required: "This value is required.",
        pattern: "This value seems to be invalid.",
        min: "This value should be greater than or equal to %s.",
        max: "This value should be lower than or equal to %s.",
        range: "This value should be between %s and %s.",
        minlength: "This value is too short. It should have %s characters or more.",
        maxlength: "This value is too long. It should have %s characters or fewer.",
        length: "This value length is invalid. It should be between %s and %s characters long.",
        mincheck: "You must select at least %s choices.",
        maxcheck: "You must select %s choices or fewer.",
        check: "You must select between %s and %s choices.",
        equalto: "This value should be the same.",
        euvatin: "It's not a valid VAT Identification Number.",
    });*/

    Parsley.setLocale('en');
    $('#registrationForm').parsley();
}
else{
}

    function LangSwitch(){
        
        let langflag = document.getElementById("langFlag");
        if(langflag.getAttribute("for") != "english"){
            langflag.setAttribute("for", "english");
            langflag.setAttribute("src", "{{asset('storage/images/united-kingdom.png')}}");
            document.getElementById("titleJS").innerHTML = "Regisztráció";
            document.getElementById("per_label").innerHTML ="Személyes adatok";
            document.getElementById("fullname").innerHTML = "Teljes név:";
            document.getElementById("firstnameJS").setAttribute("placeholder", "Keresztnév");
            document.getElementById("lastnameJS").setAttribute("placeholder", "Vezetéknév");
            document.getElementById("gender").innerHTML = "Nem";
            document.getElementById("man").innerHTML = "Férfi";
            document.getElementById("woman").innerHTML = "Nő";
            document.getElementById("other").innerHTML = "Egyéb";
            document.getElementById("birthdate").innerHTML = "Születési dátum:";
            document.getElementById("countryBirthplace").innerHTML = "Születési ország:";
            document.getElementById("BirthCountry").innerHTML = "Kérem válasszon";
            document.getElementById("nationality").innerHTML = "Állampolgárság:";
            document.getElementById("NationalityJS").innerHTML = "Kérem válasszon";
            document.getElementById("nativeLanguage").innerHTML = "Anyanyelv";
            document.getElementById("ChooseNativeLanguage").innerHTML = "Kérem válasszon";
            document.getElementById("motherFirstname").innerHTML = "Anyja keresztneve:";
            document.getElementById("motherLastname").innerHTML = "Anyja Vezetékneve:";
            document.getElementById("tajNumber").innerHTML = "Taj szám:";
            document.getElementById("educationId").innerHTML = "Oktatási azonosító:";
            document.getElementById("phone").innerHTML = "Telefonszám:";
            document.getElementById("email").innerHTML = "E-mail cím:";
            document.getElementById("address").innerHTML = "Cím:";
            document.getElementById("countryAddress").innerHTML = "Ország:";
            document.getElementById("addressCountry").innerHTML = "Kérem válasszon";
            document.getElementById("postalCodeAddress").innerHTML = "Irányítószám:";
            document.getElementById("streetAddress").innerHTML = "Utca:";
            document.getElementById("houseNumberAddress").innerHTML = "Házszám:";
            document.getElementById("studyLanguage").innerHTML = "Tanulmányi nyelv:";
            document.getElementById("ChooseStudyLanguage").innerHTML = "Kérem válasszon";
            document.getElementById("typeOfCertificate").innerHTML = "A tanúsítvány típusa:";
            document.getElementById("typeOfCertificateJS").innerHTML = "Kérem válasszon";
            document.getElementById("certificateNumber").innerHTML = "Tanúsítvány száma:";
            document.getElementById("taxIdNumber").innerHTML = "Adószám:";
            document.getElementById("accountHoldingBank").innerHTML = "Számlavezető bank:";
            document.getElementById("bankAccountNumber").innerHTML = "Bankszámlaszám:";
            document.getElementById("bankAccountHolderName").innerHTML = "Bankszámlatulajdonos neve:";
            document.getElementById("username").innerHTML = "Felhasználónév:";
            document.getElementById("usernameJS").setAttribute("placeholder", "Felhasználónév");
            document.getElementById("password").innerHTML = "Jelszó:";
            document.getElementById("passwordconfirm").innerHTML = "Jelszó megerősítése:";
            document.getElementById("regsitration").innerHTML = "Regisztráció";
            document.getElementById("signInJS").innerHTML = "Már van fiókja?   <a href=\"{{route('login')}}\" class=\"reg_link\">Jelentkezzen be</a>";       
        }
        else{
            langflag.setAttribute("for", "hungarian");
            langflag.setAttribute("src", "{{asset('storage/images/hungary.png')}}");
            document.getElementById("titleJS").innerHTML = "Registration";
            document.getElementById("per_label").innerHTML ="Personal information";
            document.getElementById("fullname").innerHTML = "Full name:";
            document.getElementById("firstnameJS").setAttribute("placeholder", "Firstname");
            document.getElementById("lastnameJS").setAttribute("placeholder", "Lastname");
            document.getElementById("gender").innerHTML = "Gender";
            document.getElementById("man").innerHTML = "Male";
            document.getElementById("woman").innerHTML = "Female";
            document.getElementById("other").innerHTML = "Other";
            document.getElementById("birthdate").innerHTML = "Date of birth:";
            document.getElementById("countryBirthplace").innerHTML = "Place of birth:";
            document.getElementById("BirthCountry").innerHTML = "Please choose";
            document.getElementById("nationality").innerHTML = "Nationality:";
            document.getElementById("NationalityJS").innerHTML = "Please choose";
            document.getElementById("nativeLanguage").innerHTML = "Native language:";
            document.getElementById("ChooseNativeLanguage").innerHTML = "Please choose";
            document.getElementById("motherFirstname").innerHTML = "Mother's forename:";
            document.getElementById("motherLastname").innerHTML = "Mother's surname:";
            document.getElementById("tajNumber").innerHTML = "Taj number:";
            document.getElementById("educationId").innerHTML = "Education ID:";
            document.getElementById("phone").innerHTML = "Phone number:";
            document.getElementById("email").innerHTML = "Email address:";
            document.getElementById("address").innerHTML = "Address:";
            document.getElementById("countryAddress").innerHTML = "Country:";
            document.getElementById("addressCountry").innerHTML = "Please choose";
            document.getElementById("postalCodeAddress").innerHTML = "Postal code:";
            document.getElementById("streetAddress").innerHTML = "Street:";
            document.getElementById("houseNumberAddress").innerHTML = "House number:";
            document.getElementById("studyLanguage").innerHTML = "Study language:";
            document.getElementById("ChooseStudyLanguage").innerHTML = "Please choose";
            document.getElementById("typeOfCertificate").innerHTML = "Type of certificate:";
            document.getElementById("typeOfCertificateJS").innerHTML = "Please choose";
            document.getElementById("certificateNumber").innerHTML = "Certificate number:";
            document.getElementById("taxIdNumber").innerHTML = "Tax number:";
            document.getElementById("accountHoldingBank").innerHTML = "Name of bank:";
            document.getElementById("bankAccountNumber").innerHTML = "Bank account number:";
            document.getElementById("bankAccountHolderName").innerHTML = "Bank account owner name:";
            document.getElementById("username").innerHTML = "Username:";
            document.getElementById("usernameJS").setAttribute("placeholder", "Username");
            document.getElementById("password").innerHTML = "Password:";
            document.getElementById("passwordconfirm").innerHTML = "Password confirmation:";
            document.getElementById("regsitration").innerHTML = "Registration";
            document.getElementById("signInJS").innerHTML = "Already have an account?   <a href=\"{{route('login')}}\" class=\"reg_link\">Sign in</a>";     

        }
    }
</script>
@endsection
