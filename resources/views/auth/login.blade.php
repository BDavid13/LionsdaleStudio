@extends('layouts.app')

@section('content')
<style>
    body {
    background-image: url("storage/images/background.png");
    background-position: center;
    background-size: cover;

}
@media (max-width: 720px){
    body{
        background-image: url("storage/images/background.png");
        background-size: contain;
        vertical-align: middle;
    }
}
.iroda_kep {
    background-image: url("storage/images/iroda.jpg");
    background-position: center;
    background-size: cover;
    
}

.container{
    position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
</style>
<div class="container">
    <div class="row d-flex justify-content-center align-items-center h-100 ">
        <div class="col-xl-10">
            <div class="card login_card d-flex">
                <div class="row g-0">
                    <div class="col-lg-6 iroda_kep">
                        <div style="background-color: rgba(240, 208, 92, 0.7); height: 100%";>
                            <!-- Logo -->
                            <img src="{{asset('storage/images/logo.png')}}" alt="Logo" class="login_logo">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-body p-md-5 ">
                            <div class="language-switch">
                                <button onclick="LangSwitch()" style="background: none; border: none">
                                <img id="langFlag" for="hungarian" src="{{asset('storage/images/hungary.png')}}" width="40px" alt="Language">
                            </div>
                            </button>
                            <form action="#" method="POST">
                                <!-- Cím -->
                                <div class="mt-5 mb-5 pb-5">
                                    <h1 id="titleJS">Login</h1>
                                </div>    

                                <!-- User -->
                                <div class="d-flex justify-content-between align-items-center mb-4">
                                    <label style="font-weight: bold; margin-right: 20px" id="usernameJS" for="Username">Username:</label>
                                    <input type="text" class="form-control btn btn-block btn-lg login_input" id="username" name="userName" placeholder="Username">
                                </div>

                                <!-- Password -->
                                <div class="d-flex justify-content-between align-items-center mb-5" style="position: relative">
                                    <label style="font-weight: bold; margin-right: 20px" id="passwordJS" for="loginpassword">Password:</label>
                                        <input type="password" class="form-control btn btn-block btn-lg login_input" id="password" name="password" placeholder="Password">
                                        <span class="login_eye">
                                            <i class="fa-solid fa-eye passwordIcon" onclick="passwordVisibility('password', this)"></i>
                                        </span>
                                </div>

                                <!-- Submit -->
                                <div class="text-center mb-5">
                                    <button type="submit" id="submitJS" class="btn btn-lg submit_button" name="submit">Login</button>
                                </div>

                                <!-- Remember/Forget? -->
                                <div class="d-flex justify-content-between mb-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="rememberme" value="#">
                                        <label class="form-check-label" id="remembermeJS" for="Remember">Remember me</label>
                                    </div>
                                        <a class="login_link" href="#" id="rememberJS">Forgot password?</a>
                                </div>

                                <!-- Register -->
                                <div class="mb-5">
                                    <p id="signupJS">
                                        No account? <a class="login_link" href="{{route('register')}}">Sign up</a> here!
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Passwordvisibility -->
<script>
    let isShown = false;
    function passwordVisibility(inputId, icon) {

        let icon1 = icon;
        let passwordInput = document.getElementById(inputId);
        if (!isShown) {
            icon1.setAttribute("class", "svg-inline--fa fa-eye-slash passwordIcon");
            passwordInput.setAttribute("type", "text");
            isShown = true;
        }
        else {
            icon1.setAttribute("class", "svg-inline--fa fa-eye passwordIcon");
            passwordInput.setAttribute("type", "password");
            isShown = false;
        }
    }

    // Language switch
    function LangSwitch(){
        
        let langflag = document.getElementById("langFlag");
        if(langflag.getAttribute("for") == "english"){
            langflag.setAttribute("for", "hungarian");
            langflag.setAttribute("src", "{{asset('storage/images/hungary.png')}}");
            document.getElementById("titleJS").innerHTML = "Login";
            document.getElementById("usernameJS").innerHTML = "Username:";
            document.getElementById("username").setAttribute("placeholder", "Username");
            document.getElementById("passwordJS").innerHTML = "Password:";
            document.getElementById("password").setAttribute("placeholder", "Password");
            document.getElementById("submitJS").innerHTML = "Login";
            document.getElementById("remembermeJS").innerHTML = "Remember me";
            document.getElementById("rememberJS").innerHTML = "Forgot password?";
            document.getElementById("signupJS").innerHTML = "No account? <a class=\"login_link\" href=\"{{route('register')}}\">Sign up</a> here!";
        }
        else{
            langflag.setAttribute("for", "english");
            langflag.setAttribute("src", "{{asset('storage/images/united-kingdom.png')}}");
            document.getElementById("titleJS").innerHTML = "Bejelentkezés";
            document.getElementById("usernameJS").innerHTML = "Név:";
            document.getElementById("username").setAttribute("placeholder", "Felhasználónév");
            document.getElementById("passwordJS").innerHTML = "Jelszó:";
            document.getElementById("password").setAttribute("placeholder", "Jelszó");
            document.getElementById("submitJS").innerHTML = "Bejelentkezés";
            document.getElementById("remembermeJS").innerHTML = "Emlékezz rám";
            document.getElementById("rememberJS").innerHTML = "Elfelejtetted a jelszavad?";
            document.getElementById("signupJS").innerHTML = "Nincs fiókod? <a class=\"login_link\" href=\"{{route('register')}}\">Regisztrálj!</a>";
        }
    }
</script>
@endsection
